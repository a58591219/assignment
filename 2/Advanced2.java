
/**
 * 在这里给出对类 swdw 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
//12000 12 0.296 1035.52
public class Advanced2 extends JFrame implements ActionListener {
    private JTextField jtfAmount			= new JTextField(10);
    private JTextField jtfTerm   		= new JTextField(10);
    private JTextField jtfRate 	= new JTextField(20);
    private JTextField jtfPay 	= new JTextField(20);
    private JButton jbCompute  			= new JButton("Enter");

    // Main method
    public static void main(String[ ] args) {
        Advanced2 frame = new Advanced2( );
        frame.pack();
        frame.setTitle("Advanced2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public Advanced2( ) {
        // Panel p1 to hold labels and text fields
        JPanel p1 = new JPanel( );
        p1.setLayout(new GridLayout(4, 2));
        p1.add(new JLabel("Loan amount($)"));
        p1.add(jtfAmount);
        p1.add(new JLabel("Loan term in month"));
        p1.add(jtfTerm);
        p1.add(new JLabel("Interest rate(%)"));
        p1.add(jtfRate);
        p1.add(new JLabel("Output monthly pay back($)"));
        p1.add(jtfPay);        
        // Add p1 to the frame
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(p1,BorderLayout.CENTER);
        this.getContentPane().add(jbCompute,BorderLayout.SOUTH);
        // Register listener
        jbCompute.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbCompute) {						
            try{
                double Amount = Double.parseDouble(jtfAmount.getText());
                int Term = Integer.parseInt(jtfTerm.getText());
                double Rate = Double.parseDouble(jtfRate.getText());
                double Pay = Double.parseDouble(jtfPay.getText());
                run(Amount,Term,Rate,Pay);
            }catch(NumberFormatException e1){
                JOptionPane.showMessageDialog(null, "Input Number Format Error !");
            }catch(Exception e2){
                JOptionPane.showMessageDialog(null, "Input Error !");
            }
        } // end of if
    } // end of ActionPerformed

    public void run(double Amount,int Term,double Rate,double Pay){
        double totalpay = 0;
        double interest = 0;
        double totalinterest = 0;
        double Repay = 0;
        double totalRepay = 0;
        JTable table = new JTable(new DefaultTableModel(new Object[]{"Period", "Monthly pay back($)","Interest($)","Repay principal($)"},0));
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for(int i=1;i<Term+1;i++){
            interest = Amount*Rate/100*Term*(Term+1-i)/78;
            totalinterest += interest;
            totalpay += Pay;
            Repay = Pay-interest;
            totalRepay += Repay;
            model.addRow(new Object[]{i, Pay, interest, Repay});
        }
        model.addRow(new Object[]{"Total", totalpay, totalinterest, totalRepay});
        JFrame f = new JFrame();
        f.setSize(550, 350);
        f.add(new JScrollPane(table));
        f.setVisible(true);
    }
} 
