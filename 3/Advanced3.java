
/**
 * 在这里给出对类 main 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Advanced3 extends JFrame implements ActionListener {
    private JTextField jtfn			= new JTextField(10);
    private JTextField jtfresult			= new JTextField(10);
    private JButton jbCompute  			= new JButton("Enter");
    // Main method
    public static void main(String[ ] args) {
        Advanced3 frame = new Advanced3( );
        frame.pack();
        frame.setTitle("Advanced3");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public Advanced3( ) {
        // Panel p1 to hold labels and text fields
        JPanel p1 = new JPanel( );
        p1.setLayout(new GridLayout(4, 2));
        p1.add(new JLabel("Input N"));
        p1.add(jtfn);
        p1.add(new JLabel("Result"));
        p1.add(jtfresult);
        jtfresult.setEditable(false);

        // Add p1 to the frame
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(p1,BorderLayout.CENTER);
        this.getContentPane().add(jbCompute,BorderLayout.SOUTH);

        // Register listener
        jbCompute.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbCompute) {						
            try{
                long getn = Long.parseLong(jtfn.getText());
                double result = 0;
                for(double i=1.0;i<getn+1;i++){
                    result += 1/i;
                }
                jtfresult.setText(""+result);
            }catch(NumberFormatException e1){
                JOptionPane.showMessageDialog(null, "Input Number Format Error !");
                jtfn.setText("");
            }catch(Exception e2){
                JOptionPane.showMessageDialog(null, "Input Error !");
                jtfn.setText("");
            }
        } // end of if
    } // end of ActionPerformed
} 