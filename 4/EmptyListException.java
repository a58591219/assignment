
/**
 * 在这里给出对类 ssc 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class EmptyListException extends RuntimeException {
   public EmptyListException(){
      this( "List" );
   }

   public EmptyListException( String name )
   {
      super( name + " is empty" );
   }
} 