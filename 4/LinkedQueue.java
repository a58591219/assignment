
/**
 * 在这里给出对类 sc 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class LinkedQueue implements Queue {
    private LinkedList  qll;
    public LinkedQueue() {
        qll = new LinkedList();
    }

    public int size() {
        return qll.getCount();
    }

    public boolean isEmpty() {
        return qll.isEmpty();
    }

    public String front() throws QueueEmptyException {
        try {
            String item = qll.removeFromHead();
            qll.addToHead(item);
            return item;
        }
        catch (EmptyListException e) {
            throw new QueueEmptyException();
        }
    }

    public void enqueue(String item) throws QueueFullException {
        qll.addToTail(item);
    }

    public String dequeue() throws QueueEmptyException {
        try {
            Object item = qll.removeFromHead();
            return ""+item;
        }
        catch (EmptyListException e) {
            throw new QueueEmptyException();
        }
    }
    
    public String toString() {
        return qll.toString();
    }
}