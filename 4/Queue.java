
/**
 * 在这里给出对类 sc 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public interface Queue {
	public abstract boolean isEmpty();
	public abstract int size();
	public abstract String front() throws QueueEmptyException;
	public abstract void enqueue(String item) throws QueueFullException;
	public abstract String dequeue() throws QueueEmptyException;
}
