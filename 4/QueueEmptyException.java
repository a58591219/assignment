
/**
 * 在这里给出对类 s 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class QueueEmptyException extends RuntimeException {
   public QueueEmptyException(){
      this( "List" );
   }

   public QueueEmptyException( String name )
   {
      super( name + " is Empty" );
   }
} 