
/**
 * 在这里给出对类 sc 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class QueueTest {
    public static void main(String [ ] args) {
        Queue queue = new LinkedQueue();
        queue.enqueue("Alex");
        queue.enqueue("Sandy");
        queue.enqueue("Betty");
        System.out.println(queue);
        System.out.println("Removed: " + queue.dequeue());
        System.out.println(queue);
        System.out.println("Removed: " + queue.dequeue());
        System.out.println(queue);
        System.out.println("Removed: " + queue.dequeue());
        System.out.println(queue);

    }
}
