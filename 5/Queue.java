
/**
 * 在这里给出对类 s 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public interface Queue {
	public abstract boolean isEmpty();
	public abstract int size();
	public abstract Object front() throws QueueEmptyException;
	public abstract void enqueue(Object item) throws QueueFullException;
	public abstract Object dequeue() throws QueueEmptyException;
}
