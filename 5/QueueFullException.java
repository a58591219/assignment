
/**
 * 在这里给出对类 s 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class QueueFullException extends RuntimeException {
   public QueueFullException(){
      this( "List" );
   }

   public QueueFullException( String name )
   {
      super( name + " is Full" );
   }
} 