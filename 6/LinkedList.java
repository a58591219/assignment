
/**
 * 在这里给出对类 sc 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class LinkedList{
    private ListNode head;
    private ListNode tail;
    private int count=0;
    public LinkedList(){
        head = null;
        tail = null;
    }

    public boolean isEmpty(){
        return (head==null);
    }

    public void addToTail ( Object  item ) {
        if (isEmpty()) {
            head = tail = new ListNode(item);
        } else {
            tail.next = new ListNode(item);
            tail =  tail.next;
        }
    }
    
    public Object GetTail () {
        if (isEmpty()) {
            return null;
        } else {
            return tail.data;
        }
    }

    public void addItemAt(Object item, int n){
        if (isEmpty() || n==0) {
            head = new ListNode(item,head);
            return;
        }
        int currentPos=0;
        ListNode current=head;
        while (currentPos < (n-1)) { 
            current = current.next;
            currentPos++;
        }
        ListNode newNode = new ListNode(item); 
        newNode.next = current.next;
        current.next = newNode;
    }
    
    public Object Getsearch(int n){
        if (isEmpty() || n==0) {
            return null;
        }
        int currentPos=0;
        ListNode current=head;
        while (currentPos < (n-1)) { 
            current = current.next;
            currentPos++;
        }
        return current.data;
    }

    public void removeItemAt(int n){
        if (n==0){
            head=head.next;
            return;
        }
        int currentPos=0;
        ListNode current=head;
        while (currentPos<n-1) {
            current = current.next;
            currentPos++;
        }
        current.next = current.next.next;
    }
    
    public Object removeFromHead(){
        Object item = null;
        if (isEmpty()) {
        } 
        item = head.data;
        if (head == tail)      // there's only one single node
            head = tail = null;
        else
            head = head.next;
        return item;
    }
    
    public void addToHead(Object item) {
        if (isEmpty()) {
            head = tail = new ListNode(item);
        } else {
            head = new ListNode(item, head);
        }
    }

    public int getCount(){
        ListNode current=head;
        count = 0;
        while (current != null) {
            count++;
            current=current.next;
        }
        return count;
    }

    public String toString() {
        String s = "[ ";
        ListNode current = head;
        while (current != null) {
            s += current.data + " ";
            current = current.next;
        }
        return s + "]";
    }
    
}