
/**
 * 在这里给出对类 s 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class LinkedStack{
	private LinkedList  sll;
	public LinkedStack() {
		sll = new LinkedList();
	}
	
	public int size() {
		return sll.getCount();
	}
	public boolean empty() {
		return sll.isEmpty();
	}
	public void push(Object item){
		sll.addToHead(item);
	}
	public Object pop() {
		try {
			Object item = sll.removeFromHead();
			return item;
		}
		catch (Exception e) {return null;}
	}
	public Object peek() {
		try {
			Object item = sll.GetTail();
			return item;
		}
		catch (Exception e) {return null;}
	}
	public Object search(int s) {
		try {
			Object item = sll.Getsearch(s);
			return item;
		}
		catch (Exception e) {return null;}
	}

}
