
/**
 * 在这里给出对类 s 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class Reverse {
	public static void main(String [ ] args) {
		String [ ] arr = {"aaa", "bbb", "ccc", "ddd", "eee","fff","ggg"};
		LinkedStack stack = new LinkedStack();
		System.out.println(stack.empty());
		for (int i=0; i<arr.length; i++) {
			stack.push(arr[i]);
		}
		System.out.println(stack.peek());
		System.out.println(stack.empty());
		System.out.println(stack.size());
		System.out.println(stack.search(2));
		for (int i=0; i<arr.length; i++) {
			arr[i] = (String) stack.pop();
		}
		for (int i=0; i<arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
