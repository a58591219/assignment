
/**
 * 在这里给出对类 d 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Advanced7a extends JFrame implements ActionListener {
    private JTextField jtfFirst			= new JTextField(10);
    private JButton jbCompute  			= new JButton("Enter");
    private String getword[];
    // Main method
    //LinkedList input = new LinkedList();
    DoublyList input2 = new DoublyList();
    public static void main(String[ ] args) {
        Advanced7a frame = new Advanced7a( );
        frame.pack();
        frame.setTitle("Advanced7");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public Advanced7a( ) {
        // Panel p1 to hold labels and text fields
        JPanel p1 = new JPanel( );
        p1.setLayout(new GridLayout(4, 2));
        p1.add(new JLabel("Input:"));
        p1.add(jtfFirst);

        // Add p1 to the frame
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(p1,BorderLayout.CENTER);
        this.getContentPane().add(jbCompute,BorderLayout.SOUTH);

        // Register listener
        jbCompute.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbCompute) {						
            try{
                getword = jtfFirst.getText().split(" ");
                run();
            }catch(NumberFormatException e1){
                JOptionPane.showMessageDialog(null, "Input Number Format Error !");
                jtfFirst.setText("");
            }//catch(Exception e2){
            //JOptionPane.showMessageDialog(null, "Input Error !");
            //jtfFirst.setText("");
            //}
        } // end of if
    } // end of ActionPerformed

    public void run(){
        for(int i=0;i<getword.length;i++){
            String output="";
            for(int ii=0;ii<getword[i].length();ii++){
                char c=getword[i].charAt(ii);
                if((c>='A'&&c<='Z')||(c>='a'&& c<='z')||c=='\''||c=='’'||c=='-'){
                    output = output + c;
                }
            }
            if(output.equals("")){
            }else{
                input2.addToTail(output);
                //System.out.println(input);
            }
        }
        //input.swapped(0,4);
        System.out.println("Original: "+input2);
        //input2.swapped(3,4);
        //System.out.println("ok: "+input2);
        num();
    }

    public void num(){
        int count = input2.getCount();
        //try{
        for(int i=0;i<count-1;i++){
            for(int ii=i;ii<count;ii++){
                if(input2.getstring(i).length()>input2.getstring(ii).length()){
                    try{
                        input2.swapped(i+1,ii+1);
                    }catch(Exception e){}
                }
                //System.out.println(input2);
            }
        }
        //}catch(Exception e){}
        //System.out.println(input);
        //input.getstring(i).toUpperCase().charAt(0)>input.getstring(ii).toUpperCase().charAt(0) ||
        //System.out.println("Change1: "+input2);
        letter();
    }

    public void letter(){
        int count = input2.getCount();
        try{
            for(int i=0;i<count-1;i++){
                for(int ii=i;ii<count;ii++){
                    for(int iii=0;iii<input2.getstring(i).length();iii++){
                        if(input2.getstring(i).toUpperCase().charAt(iii)>input2.getstring(ii).toUpperCase().charAt(iii) && input2.getstring(i).length()==input2.getstring(ii).length()){
                            //System.out.println(input2+" "+input2.getstring(i).toUpperCase().charAt(iii)+" "+input2.getstring(ii).toUpperCase().charAt(iii));
                            input2.swapped(i+1,ii+1);
                            break;
                        }
                        if(input2.getstring(i).toUpperCase().charAt(iii)<input2.getstring(ii).toUpperCase().charAt(iii) && input2.getstring(i).length()==input2.getstring(ii).length()){
                            break;
                        }
                        //System.out.println("L"+input2);
                    }
                }
            }
        }catch(Exception e){}
        System.out.println("Change: "+input2);
    }
}