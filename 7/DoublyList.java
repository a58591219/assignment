
/**
 * 在这里给出对类 s 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class DoublyList {
    private DoublyNode head;
    private DoublyNode tail;
    private DoublyNode next, prev;
    private int count=0;
    public DoublyList() {
        head = tail = null;
    }

    public boolean isEmpty() {
        return (head==null);
    }

    public void addToHead(String item) {
        if (isEmpty()) {
            head = tail = new DoublyNode(item);
        } else {
            head.previous = new DoublyNode(item, null, head);
            head = head.previous;
        }
    }

    public void addToTail(String item) {
        if (isEmpty()) {
            head = tail = new DoublyNode(item);
        } else {
            tail.next = new DoublyNode(item, tail, null);
            tail = tail.next;
        }
    }

    public String removeFromHead() throws EmptyListException {
        if (isEmpty()) {
            throw new EmptyListException();
        } 
        String item = head.data.toString();
        if (head == tail) 
            head = tail = null;
        else {
            head = head.next;
            head.previous = null;
        }
        return item;
    }

    public String removeFromTail() throws EmptyListException {
        if (isEmpty()) {
            throw new EmptyListException();
        } 
        String item = tail.data.toString();
        if (head == tail) {
            head = tail = null;
        } else {
            tail = tail.previous;
            tail.next = null;
        }
        return item;
    }

    public void addItemAt(String item, int n){
        if (isEmpty() || n==0) {
            head = tail = new DoublyNode(item);
            return;
        }
        int currentPos=0;
        DoublyNode current=head;
        while (currentPos < (n-1)) { 
            current = current.next;
            currentPos++;
        }
        //DoublyNode newNode = new DoublyNode(item);
        //current.next = newNode;
        //newNode.next = current.next;
        //current.previous.next = newNode;

        DoublyNode newNode = new DoublyNode(item);
        //newNode.next = current;
        current.previous.next = newNode;
        current.previous = newNode;
        newNode.previous = current.previous;
    }
  
    public void removeItemAt(int n){
        if (n==0){
            head=head.next;
            return;
        }
        int currentPos=0;
        DoublyNode current=head;
        while (currentPos<n-1) {
            current = current.next;
            currentPos++;
        }
        //current.next = current.next.next;

        DoublyNode q=current.previous;
        q.next=current.next;
        current.next.previous=q;
    }

    public int getCount(){
        DoublyNode current=head;
        count = 0;
        while (current != null) {
            count++;
            current=current.next;
        }
        return count;
    }

    public String toString() {
        String s = "[ ";
        DoublyNode current = head;
        while (current != null) {
            s += current.data + " ";
            current = current.next;
        }
        return s + "]";
    }

    public String getstring(int n){
        int currentPos=0;
        DoublyNode current=head;
        while (currentPos<n) {
            current = current.next;
            currentPos++;
        }
        return current.data;
    }

    public void swapped(int left,int right){
        //String getleft = getstring(left);
        //String getright = getstring(right);
        //removeItemAt(left);
        //removeItemAt(right-1);
        //addItemAt(getright, left);
        //addItemAt(getleft, right);
        if(left==0){
            left=1;
        }
        if(right-left==1){
            change(left-1,left,right,right+1);
        }else{
            for(int i=0;i<right-left;i++){
                change(left+i-1,left+i,left+i+1,left+i+2);
            }
            //System.out.println(toString());
            for(int i=right-left-1;i>0;i--){
                change(left+i-2,left+i-1,left+i,left+i+1);
            }
        }
    }

    public void change(int f,int left,int right,int l){
        //String getleft = getstring(left);
        //String getright = getstring(right);
        //removeItemAt(left);
        //removeItemAt(right-1);
        //addItemAt(getright, left);
        //addItemAt(getleft, right);
        int currentPos=0;
        DoublyNode fa=head;
        while (currentPos<f-1) {
            fa = fa.next;
            currentPos++;
        }
        currentPos=0;
        DoublyNode righta=head;
        while (currentPos<right-1) {
            righta = righta.next;
            currentPos++;
        }
        currentPos=0;
        DoublyNode la=head;
        while (currentPos<l-1) {
            la = la.next;
            currentPos++;
        }
        currentPos=0;
        DoublyNode lefta=head;
        while (currentPos<left-1) {
            lefta = lefta.next;
            currentPos++;
        }

        if(lefta.next.next !=null)                                       // if thirdword no is null
            la.previous = lefta;                                      // the thirdword of previous Node to moveword
        else
            la = null;
        righta.next = lefta; 
        // the secondword of next node to moveword
        if(lefta == head){                                               // if moveword is head
            righta.previous = null;
            head = righta;                                              // head is secondword
        }else if(righta != tail){
            righta.previous = la.previous;                                // the secondword of previous Node to moveword of previous Node
            fa.next = righta;                                          // the firstword of next Node to secondword
        }
        if(righta == tail){   
            String a = tail.data;
            String b = tail.previous.data;
            removeFromTail();
            removeFromTail();
            addToTail(a);
            addToTail(b);
        }else {
            lefta.next = la;   
        }// the moveword of next Node to thirdword
        lefta.previous = righta;   
    }

}
