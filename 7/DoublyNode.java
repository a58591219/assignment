
/**
 * 在这里给出对类 s 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class DoublyNode {
	public String data;
	public DoublyNode previous;
	public DoublyNode next;

	public DoublyNode(String data) {
		this.data = data;
		this.previous = null;
		this.next = null;
	}

	public DoublyNode(String data, DoublyNode previous, DoublyNode next) {
		this.data = data;
		this.previous = previous;
		this.next = next;
	}
}
