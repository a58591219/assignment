
/**
 * 在这里给出对类 ss 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Advanced8 extends JFrame implements ActionListener {
    private JTextField jtfn			= new JTextField(10);
    private JTextField jtfresult			= new JTextField(10);
    private JButton jbCompute  			= new JButton("Enter");
    // Main method
    public static void main(String[ ] args) {
        Advanced8 frame = new Advanced8( );
        frame.pack();
        frame.setTitle("Advanced8");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public Advanced8( ) {
        // Panel p1 to hold labels and text fields
        JPanel p1 = new JPanel( );
        p1.setLayout(new GridLayout(4, 2));
        p1.add(new JLabel("Input binary"));
        p1.add(jtfn);
        p1.add(new JLabel("Result"));
        p1.add(jtfresult);
        jtfresult.setEditable(false);

        // Add p1 to the frame
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(p1,BorderLayout.CENTER);
        this.getContentPane().add(jbCompute,BorderLayout.SOUTH);

        // Register listener
        jbCompute.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbCompute) {						
            try{
                String getn = jtfn.getText();
                boolean ck = true;
                for(int i=0;i<getn.length();i++){
                    int text = Integer.parseInt(getn.substring(i,i+1).toString());
                    if(text>1){
                        ck = false;
                    }
                }
                if(ck){
                    run(getn);
                }else{
                    JOptionPane.showMessageDialog(null, "Input Number > 1 !");
                }
            }catch(NumberFormatException e1){
                JOptionPane.showMessageDialog(null, "Input Number Format Error !");
                jtfn.setText("");
            }catch(Exception e2){
                JOptionPane.showMessageDialog(null, "Input Error !");
                jtfn.setText("");
            }
        } // end of if
    } // end of ActionPerformed

    public void run(String get){
        int count = get.length();
        int result = 0;
        String text = "";
        int barray[] = new int[get.length()];
        for(int i=0;i<get.length();i++){
            barray[i] = Integer.parseInt(get.substring(i,i+1));
            result += barray[i]*Math.pow(2,count-1);
            text += (Integer.parseInt(get.substring(i,i+1))*Math.pow(2,count-1));
            count--;
        }
        //jtfresult.setText(""+text);
        jtfresult.setText(""+result);
    }
}