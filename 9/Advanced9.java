
/**
 * 在这里给出对类 df 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Advanced9 extends JFrame implements ActionListener {
    private JTextField jtfn			= new JTextField(10);
    private JButton jbCompute  			= new JButton("Enter");
    // Main method
    public static void main(String[ ] args) {
        Advanced9 frame = new Advanced9( );
        frame.pack();
        frame.setTitle("Advanced9");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public Advanced9( ) {
        // Panel p1 to hold labels and text fields
        JPanel p1 = new JPanel( );
        p1.setLayout(new GridLayout(4, 2));
        p1.add(new JLabel("Input Number of Fibonacci sequence"));
        p1.add(jtfn);

        // Add p1 to the frame
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(p1,BorderLayout.CENTER);
        this.getContentPane().add(jbCompute,BorderLayout.SOUTH);

        // Register listener
        jbCompute.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbCompute) {						
            try{
                long getn = Long.parseLong(jtfn.getText());
                String result = "0 1 ";
                if(getn==1){
                    result = "0";
                }
                int a = 0;
                int b = 1;
                int c = 0;
                for(int i=3;i<=getn;i++){
                    c = a+b;
                    result += c+" ";
                    a = b;
                    b = c;
                }
                System.out.println(result);
            }catch(NumberFormatException e1){
                JOptionPane.showMessageDialog(null, "Input Number Format Error !");
                jtfn.setText("");
            }catch(Exception e2){
                JOptionPane.showMessageDialog(null, "Input Error !");
                jtfn.setText("");
            }
        } // end of if
    } 
} 