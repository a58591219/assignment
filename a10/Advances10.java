
/**
 * 在这里给出对类 Advances10 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class Advances10{
    private int sortarrary[] = null;
    private static int finsortnum = 0;
    public static void main(String[] args) {
        //int sortarrary[] = new int[]{0,8,38,93,39,21,26,63,11,32,2,3,4,5,77,33};
        int sortarrary[] = new int[]{0,8,38,93,39,21,26,63,11,32};

        //Building heap;
        printout(sortarrary);
        int startheap = (int)Math.floor(sortarrary.length/2.0);
        for(int i=startheap;i>0;i--){
            toc(i,sortarrary);
        }
        //printout(sortarrary);
        //Heapsort 93 39 63 38 21 26 8 11 32
        int max = 1;
        for(int ii=1;ii<sortarrary.length;ii++){
            if(sortarrary[ii]>sortarrary[max]){
                max = ii;
            }
        }
        swap(max,sortarrary.length-1,sortarrary);
        //sort
        for(int i=sortarrary.length-2;i>1;i--){
            toa(1,sortarrary,i);
            swap(1,i,sortarrary);
            //printout(sortarrary);
        }
        printout(sortarrary);
        System.out.println("Swap:" + finsortnum);
        System.out.println("");
    }

    public static void swap(int a,int b,int[] sortarrary){
        int geta = sortarrary[a];
        int getb = sortarrary[b];
        sortarrary[a] = getb;
        sortarrary[b] = geta;  
        finsortnum++;
    }

    public static void toa(int num,int[] sortarrary,int maxarray){
        int cleft=0;
        int cright=0;
        try{
            cleft = sortarrary[num*2];
            cright = sortarrary[num*2+1];
        }catch(Exception e){}
        if(sortarrary[num]<cleft && (cleft>cright||maxarray==2) && maxarray>=num*2){
            swap(num,num*2,sortarrary);
            toa(num*2,sortarrary,maxarray);
        }else if(sortarrary[num]<cright && cleft<cright && maxarray>num*2+1){
            swap(num,num*2+1,sortarrary);
            toa(num*2+1,sortarrary,maxarray);
        }
    }

    public static void toc(int num,int[] sortarrary){
        int cleft=0;
        int cright=0;
        try{
            cleft = sortarrary[num*2];
            cright = sortarrary[num*2+1];
        }catch(Exception e){}
        if(sortarrary[num]<cleft && cleft>cright){
            swap(num,num*2,sortarrary);
            toc(num*2,sortarrary);
        }else if(sortarrary[num]<cright && cleft<cright){
            swap(num,num*2+1,sortarrary);
            toc(num*2+1,sortarrary);
        }
    }
    
    public static void printout(int[] sortarrary){
        for(int i=1;i<sortarrary.length;i++){
            System.out.print(sortarrary[i]+" ");
        }
        System.out.println("");
    }
}
