
/**
 * 在这里给出对类 ss 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class Advances11{
    private int sortarrary[] = null;
    private static int finsortnum = 0;
    private static boolean fin = true;
    public static void main(String[] args) {
        //int sortarrary[] = new int[]{0,8,38,93,39,21,26,63,11,32,2,3,4,5,77,33};
        int sortarrary[] = new int[]{0,8,38,93,39,21,26,63,11,32};

        printout(sortarrary);
        while(fin){
            fin = false;
            for(int i=1;i<sortarrary.length-1;i++){
                if(sortarrary[i]>sortarrary[i+1]){
                    swap(i,i+1,sortarrary);
                }
            }
        }
        
        printout(sortarrary);
        System.out.println("Swap: " + finsortnum);
        System.out.println("");
    }

    public static void swap(int a,int b,int[] sortarrary){
        int geta = sortarrary[a];
        int getb = sortarrary[b];
        sortarrary[a] = getb;
        sortarrary[b] = geta;  
        fin = true;
        finsortnum++;
    }

     public static void printout(int[] sortarrary){
        for(int i=1;i<sortarrary.length;i++){
            System.out.print(sortarrary[i]+" ");
        }
        System.out.println("");
    }
}